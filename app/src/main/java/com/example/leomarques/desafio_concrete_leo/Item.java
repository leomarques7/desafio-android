package com.example.leomarques.desafio_concrete_leo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by leomarques on 9/23/16.
 */
public class Item implements Serializable {
    String name, description, forks;
    @SerializedName("stargazers_count")
    String stars;

    Owner owner;
}
