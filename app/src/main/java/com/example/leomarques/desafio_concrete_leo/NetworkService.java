package com.example.leomarques.desafio_concrete_leo;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by leomarques on 9/23/16.
 */
public interface NetworkService {
    @GET("/search/repositories?q=language:Java&sort=stars")
    void listRepos(@Query("page") int page, Callback<ReposList> cb);

    @GET("/repos/{owner}/{repo}/pulls")
    void listPulls(@Path("owner") String owner, @Path("repo") String repo, Callback<List<PullRequest>> cb);
}
