package com.example.leomarques.desafio_concrete_leo;

import android.app.Application;

import retrofit.RestAdapter;

/**
 * Created by leomarques on 9/26/16.
 */
public class App extends Application {
    private static NetworkService networkService;
    private static App inst;

    public App() {
        inst = this;
    }

    public static App inst() {
        return inst;
    }

    public static NetworkService getNetworkService() {
        if (networkService == null) {
            networkService = new RestAdapter.Builder()
                    .setEndpoint("https://api.github.com")
                    .setLogLevel(RestAdapter.LogLevel.BASIC)
                    .build()
                    .create(NetworkService.class);
        }

        return networkService;
    }

}
