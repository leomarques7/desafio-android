package com.example.leomarques.desafio_concrete_leo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by leomarques on 9/27/16.
 */
public class User implements Serializable {
    @SerializedName("avatar_url")
    String avatarUrl;
    String login;
}
