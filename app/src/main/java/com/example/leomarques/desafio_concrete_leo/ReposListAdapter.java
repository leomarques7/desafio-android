package com.example.leomarques.desafio_concrete_leo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by leomarques on 9/23/16.
 */
public class ReposListAdapter extends RecyclerView.Adapter<ReposListAdapter.ViewHolder> {

    private final List<Item> itemsList;
    private final Context context;

    public ReposListAdapter(Context context, List<Item> itemsList) {
        this.context = context;
        this.itemsList = itemsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_repo_item, parent, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Item item = itemsList.get(position);

        holder.name.setText(item.name);
        holder.description.setText(item.description);
        holder.forks.setText(item.forks);
        holder.stars.setText(item.stars);
        holder.login.setText(item.owner.login);

        Picasso.with(context).load(item.owner.avatarUrl).into(holder.avatar);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PullActivity.class);
                intent.putExtra("login", item.owner.login);
                intent.putExtra("name", item.name);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void addItems(ReposList listItems) {
        itemsList.addAll(listItems.items);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, description, forks, stars, login;
        ImageView avatar;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            description = (TextView) itemView.findViewById(R.id.description);
            forks = (TextView) itemView.findViewById(R.id.forks);
            stars = (TextView) itemView.findViewById(R.id.stars);
            login = (TextView) itemView.findViewById(R.id.login);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}
