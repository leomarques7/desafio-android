package com.example.leomarques.desafio_concrete_leo;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PullActivity extends AppCompatActivity {

    public static final String PULLS_LIST_RESPONSE = "pullsListResponse";

    @BindView(R.id.pbar)
    public ProgressBar pbar;

    @BindView(R.id.recycler_view)
    public RecyclerView pullsList;

    private ArrayList<PullRequest> pullsListResponse;
    private PullListAdapter adapter;
    private String login, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);
        ButterKnife.bind(this);

        login = getIntent().getStringExtra("login");
        name = getIntent().getStringExtra("name");

        if (savedInstanceState != null) {
            pullsListResponse = savedInstanceState.getParcelableArrayList(PULLS_LIST_RESPONSE);

            populateList(pullsListResponse);
        } else {
            requestPulls();
        }

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(name);
    }

    private void requestPulls() {
        pbar.setVisibility(View.VISIBLE);

        App.getNetworkService().listPulls(login, name, new Callback<List<PullRequest>>() {
            @Override
            public void success(List<PullRequest> reposListResponse, Response response) {
                populateList((ArrayList<PullRequest>) reposListResponse);

                pbar.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("PullActivity", error != null ? error.getMessage() : "Error - listPulls");

                pbar.setVisibility(View.GONE);
            }
        });
    }

    private void populateList(ArrayList<PullRequest> listItems) {
        pullsListResponse = listItems;

        if (adapter != null) {
            adapter.addItems(listItems);
        } else {
            adapter = new PullListAdapter(this, listItems);
            pullsList.setAdapter(adapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            pullsList.setLayoutManager(linearLayoutManager);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (pullsListResponse == null)
            return;
        savedInstanceState.putParcelableArrayList(PULLS_LIST_RESPONSE, pullsListResponse);

        super.onSaveInstanceState(savedInstanceState);
    }
}
