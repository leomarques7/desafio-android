package com.example.leomarques.desafio_concrete_leo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by leomarques on 9/23/16.
 */
public class PullListAdapter extends RecyclerView.Adapter<PullListAdapter.ViewHolder> {

    private final List<PullRequest> itemsList;
    private final Context context;

    public PullListAdapter(Context context, List<PullRequest> itemsList) {
        this.context = context;
        this.itemsList = itemsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_pull_request, parent, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PullRequest item = itemsList.get(position);

        holder.name.setText(item.title);
        holder.description.setText(item.body);
        holder.login.setText(item.user.login);

        Picasso.with(context).load(item.user.avatarUrl).into(holder.avatar);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.htmlUrl));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void addItems(List<PullRequest> listItems) {
        itemsList.addAll(listItems);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, description, login;
        ImageView avatar;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            description = (TextView) itemView.findViewById(R.id.description);
            login = (TextView) itemView.findViewById(R.id.login);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }
}
