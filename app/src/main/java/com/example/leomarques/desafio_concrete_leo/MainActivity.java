package com.example.leomarques.desafio_concrete_leo;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    public static final String REPOS_LIST_RESPONSE = "reposListResponse";

    @BindView(R.id.pbar)
    public ProgressBar pbar;

    @BindView(R.id.recycler_view)
    public RecyclerView reposList;

    private ReposList reposListResponse;
    private ReposListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null)
            supportActionBar.setTitle(R.string.repoact_title);

        if (savedInstanceState != null) {
            ReposList reposList = (ReposList) savedInstanceState.getSerializable(REPOS_LIST_RESPONSE);
            populateList(reposList);
        } else {
            requestRepos(1);
        }
    }

    private void requestRepos(int page) {
        pbar.setVisibility(View.VISIBLE);

        App.getNetworkService().listRepos(page, new Callback<ReposList>() {
            @Override
            public void success(ReposList reposListResponse, Response response) {
                populateList(reposListResponse);

                pbar.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("MainActivity", error != null ? error.getMessage() : "Error - listRepos");

                pbar.setVisibility(View.GONE);
            }
        });
    }

    private void populateList(ReposList listItems) {
        reposListResponse = listItems;

        if (adapter != null) {
            adapter.addItems(listItems);
        } else {
            adapter = new ReposListAdapter(this, listItems.items);
            reposList.setAdapter(adapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            reposList.setLayoutManager(linearLayoutManager);
            reposList.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    requestRepos(page + 1);
                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (reposListResponse == null)
            return;
        savedInstanceState.putSerializable(REPOS_LIST_RESPONSE, reposListResponse);

        super.onSaveInstanceState(savedInstanceState);
    }
}
