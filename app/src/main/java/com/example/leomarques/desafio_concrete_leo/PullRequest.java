package com.example.leomarques.desafio_concrete_leo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by leomarques on 9/27/16.
 */
public class PullRequest implements Parcelable {
    @SerializedName("html_url")
    String htmlUrl;
    @SerializedName("created_at")
    String createdAt;
    String title, body;
    User user;

    protected PullRequest(Parcel in) {
        htmlUrl = in.readString();
        createdAt = in.readString();
        user = (User) in.readValue(User.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(htmlUrl);
        dest.writeString(createdAt);
        dest.writeValue(user);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PullRequest> CREATOR = new Parcelable.Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel in) {
            return new PullRequest(in);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };
}